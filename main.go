package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"lesterpig.com/upspin-feed/feed"
	"lesterpig.com/upspin-feed/server"
	"lesterpig.com/upspin-feed/store"
	"upspin.io/config"
	"upspin.io/flags"

	_ "upspin.io/key/transports"
	_ "upspin.io/pack/plain"
	_ "upspin.io/transports"
)

func main() {
	path := flag.String("path", "/tmp/upspin-feed", "Upspin-Feed storage directory")
	flags.Parse(flags.Server)

	cfg, err := config.FromFile(flags.Config)
	if err != nil {
		log.Fatal(err)
	}

	s, err := store.NewStore(*path)
	if err != nil {
		log.Fatal(err)
	}

	f, err := feed.NewFeed(s)
	if err != nil {
		log.Fatal(err)
	}

	signals := make(chan os.Signal, 1)
	signal.Notify(
		signals,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)

	go func() {
		<-signals
		_ = f.Close()
		_ = s.Close()
		os.Exit(0)
	}()

	_ = server.Run(cfg, s, f)
}
