# upspin-feed

*Your daily feed reader in Upspin name space 📚*

This project provides a combined Upspin {Dir,Store}Server able to fetch a list of RSS/ATOM feeds.
It stores the fetched articles in an embedded [Badger](https://github.com/dgraph-io/badger) database.

Check the live demo at [l+feed@lesterpig.com/today](https://upspin-feed.lesterpig.com/today)

## Installation

```bash
go get lesterpig.com/upspin-feed
```

The `upspin-feed` command provides the same flags as official Upspin DirServer, plus a `path` parameter that might be used to set the location of the local database.

## Test

You can easily setup a test Upspin environment using the `upbox` command that comes with Upspin.

```bash
upbox -schema feed.upbox
ls
```

## Usage

The list of RSS/ATOM feeds is available from the Upspin tree at `<upspinusername>/feeds.csv`.
It has the following format:

```csv
Name,FeedUrl,[Category1],[Category2],[CategoryN],
```

You can easily edit this file by using standard Upspin tools (`upspinfs` or `upspin put -in feeds.csv` for instance).
After a successful update of the `feeds.csv` file, the new feeds are immediately fetched and available.
Please note that this is the only file that can be modified in the tree.

*(For a live example, see [l+feed@lesterpig.com/feeds.csv](https://upspin-feed.lesterpig.com/feeds.csv))*

## Privacy considerations

⚠️ Currently, the whole Upspin tree is world-readable, and only the plain packer is used.
However, the only user that can update the list of feeds is the one that is used to start the server.

## Contributing

Contributions welcome! Just submit a Merge Request in the Gitlab repository.
