// Package feed is a Atom/RSS news feed fetcher.
package feed

import (
	"bytes"
	"encoding/csv"
	"io"
	"strings"
)

// Feed is a RSS/ATOM feeds scraper for a list of Source (Sources).
// It shall provide a scheduler to automatically refresh data.
type Feed interface {
	io.Closer

	GetSources() (Sources, error)
	PutSources(Sources) error
}

// Sources holds multiple Source, and might be used for easy (un)marshalling
type Sources []Source

// Source represents one RSS/ATOM feed.
type Source struct {
	Name       string
	Link       string
	Categories []string
}

// MarshalBinary is used to store Sources
func (s Sources) MarshalBinary() (data []byte, err error) {
	buf := &bytes.Buffer{}
	writer := csv.NewWriter(buf)

	for _, source := range s {
		err := writer.Write([]string{
			source.Name,
			source.Link,
			strings.Join(source.Categories, "/"),
		})

		if err != nil {
			return nil, err
		}
	}

	writer.Flush()
	return buf.Bytes(), nil
}

// UnmarshalBinary is used to retrieve Sources from store
func (s *Sources) UnmarshalBinary(data []byte) error {
	reader := csv.NewReader(bytes.NewReader(data))
	records, err := reader.ReadAll()
	if err != nil {
		return err
	}

	for _, record := range records {
		if len(record) < 3 {
			continue
		}

		*s = append(*s, Source{
			Name:       record[0],
			Link:       record[1],
			Categories: strings.Split(record[2], "/"),
		})
	}

	return nil
}
