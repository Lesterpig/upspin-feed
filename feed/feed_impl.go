package feed

import (
	"errors"
	"log"
	"runtime"
	"time"

	"github.com/miniflux/miniflux/reader/scraper"
	"github.com/mmcdole/gofeed"
	"lesterpig.com/upspin-feed/store"
)

const (
	indexKey     = "index"
	tickInterval = 15 * time.Minute
)

type httpFeed struct {
	store             store.Store
	tasks             chan Source
	close             chan bool
	indexKeyReference string
}

// NewFeed returns a Feed able to fetch data with HTTP requests,
// and uses the provided store to save its data.
func NewFeed(store store.Store) (Feed, error) {
	if store == nil {
		return nil, errors.New("invalid nil store")
	}

	feed := &httpFeed{
		store:             store,
		tasks:             make(chan Source),
		close:             make(chan bool),
		indexKeyReference: store.GetReference("", indexKey),
	}

	go feed.scheduler()

	return feed, nil
}

func (f *httpFeed) Close() error {
	// Check that this feed is not already closed
	select {
	case <-f.close:
	default:
		close(f.close)
	}

	return nil
}

func (f *httpFeed) GetSources() (sources Sources, err error) {
	data, err := f.store.Get(f.indexKeyReference)
	if err != nil {
		if _, ok := err.(*store.ErrorReferenceNotFound); ok {
			err = nil
		}
		return
	}

	err = sources.UnmarshalBinary(data.Data)
	return
}

func (f *httpFeed) PutSources(sources Sources) error {
	data, err := sources.MarshalBinary()
	if err != nil {
		return err
	}

	// Schedule new tasks
	go func() {
		for _, source := range sources {
			select {
			case f.tasks <- source:
			case <-f.close:
				return
			}
		}
	}()

	_, err = f.store.Put("", indexKey, data)
	return err
}

func (f *httpFeed) lastUpdate(link string) time.Time {
	data, _ := f.store.Get(f.store.GetReference("", "update/"+link))

	var update time.Time
	_ = update.UnmarshalBinary(data.Data)
	return update
}

func (f *httpFeed) setUpdate(link string, t time.Time) error {
	data, _ := t.MarshalBinary()
	_, err := f.store.Put("", "update/"+link, data)
	return err
}

func (f *httpFeed) scheduler() {
	// Start workers
	for i := 0; i < runtime.GOMAXPROCS(0); i++ {
		go f.worker()
	}

	ticker := time.NewTicker(tickInterval)
	for {
		sources, _ := f.GetSources()
		for _, source := range sources {
			select {
			case f.tasks <- source:
			case <-f.close:
				ticker.Stop()
				close(f.tasks)
				return
			}
		}

		select {
		case <-f.close:
			ticker.Stop()
			close(f.tasks)
			return
		case <-ticker.C:
		}
	}
}

func (f *httpFeed) worker() {
	parser := gofeed.NewParser()
	for source := range f.tasks {
		feed, err := parser.ParseURL(source.Link)
		if err != nil {
			log.Println("unable to get", source.Link, ":", err)
			continue
		}

		// Compare lastUpdate with fetched date
		if feed.UpdatedParsed != nil && !f.lastUpdate(source.Link).Before(*feed.UpdatedParsed) {
			continue
		}

		for _, item := range feed.Items {
			if !f.lastUpdate(item.Link).Before(getSafeItemDate(item)) {
				continue
			}

			err = f.saveItem(source, item)
			if err != nil {
				log.Println("unable to save item", item.Title)
			}
		}

		// Save lastUpdate
		if feed.UpdatedParsed != nil {
			err = f.setUpdate(source.Link, *feed.UpdatedParsed)
			if err != nil {
				log.Println("unable to save last update for", source.Link)
			}
		}
	}
}

func (f *httpFeed) saveItem(source Source, item *gofeed.Item) error {
	category := item.PublishedParsed.Format("2006-01-02")
	title := source.Name + " - " + item.Title

	var err error
	if len(item.Content) == 0 {
		time.Sleep(time.Second)
		item.Content, err = scraper.Fetch(item.Link, "")
		if err != nil {
			return err
		}
	}

	data := []byte(`<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>
` + item.Content + `
	</body>
</html>`)

	_, err = f.store.Put(category, title, data)
	if err != nil {
		return err
	}

	log.Println("new article", category, title)
	return f.setUpdate(item.Link, getSafeItemDate(item))
}

func getSafeItemDate(item *gofeed.Item) time.Time {
	if item.UpdatedParsed != nil {
		return *item.UpdatedParsed
	}

	if item.PublishedParsed != nil {
		return *item.PublishedParsed
	}

	return time.Time{}
}
