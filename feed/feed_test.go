package feed

import (
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"lesterpig.com/upspin-feed/store"
)

func TestSourcesMarshaling(t *testing.T) {
	sources := Sources{
		{Name: "Source A", Link: "Link A", Categories: []string{""}},
		{Name: "Source B", Link: "Link B", Categories: []string{"Category 1", "Category 2"}},
	}

	data, err := sources.MarshalBinary()
	require.Nil(t, err)

	var retrievedSources Sources
	err = retrievedSources.UnmarshalBinary(data)
	require.Nil(t, err)
	require.Equal(t, sources, retrievedSources)
}

func TestFeed(t *testing.T) {
	path, err := ioutil.TempDir("", "")
	require.Nil(t, err)

	defer func() {
		_ = os.RemoveAll(path)
	}()

	s, err := store.NewStore(path)
	require.Nil(t, err)

	f, err := NewFeed(s)
	require.Nil(t, err)

	defer func() {
		_ = f.Close()
		_ = s.Close()
	}()

	time.Sleep(time.Second)

	// Populate sources
	sources := Sources{
		{Name: "Lesterpig", Link: "http://blog.lesterpig.com/index.xml"},
		{Name: "Golang", Link: "https://blog.golang.org/feed.atom"},
	}

	err = f.PutSources(sources)
	require.Nil(t, err)

	time.Sleep(5 * time.Second)
}
