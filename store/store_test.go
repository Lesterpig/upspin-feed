package store

import (
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestBadgerStore(t *testing.T) {
	path, err := ioutil.TempDir("", "")
	require.Nil(t, err)

	defer func() {
		_ = os.RemoveAll(path)
	}()

	s, err := NewStore(path)
	require.Nil(t, err)

	defer func() {
		_ = s.Close()
	}()

	// Put and Get a simple value
	data := []byte("Hello World!")
	entry, err := s.Put("", "test", data)
	require.Nil(t, err)
	require.Equal(t, uint64(len(data)), entry.Length)

	retrievedEntry, err := s.Get(entry.Reference)
	require.Nil(t, err)

	entry.LastUpdate = entry.LastUpdate.Round(time.Nanosecond)
	require.Equal(t, entry, retrievedEntry)

	// Put multiple values into categories and try to list entries
	_, err = s.Put("A", "1", []byte("A1_data"))
	require.Nil(t, err)
	_, err = s.Put("B", "1", []byte("B1_data"))
	require.Nil(t, err)
	_, err = s.Put("A", "2_______", []byte("A2_data"))
	require.Nil(t, err)

	categories, err := s.Categories()
	require.Nil(t, err)
	require.Len(t, categories, 2)

	categories[0].LastUpdate = time.Time{}
	categories[1].LastUpdate = time.Time{}
	require.Equal(t, []Category{
		{Name: "A", Sequence: int64(4)},
		{Name: "B", Sequence: int64(3)},
	}, categories)

	entries, err := s.List("A")
	require.Nil(t, err)
	require.NotNil(t, entries)
	require.Len(t, entries, 2)
	require.Equal(t, uint64(7), entries[0].Length)
	require.Equal(t, uint64(7), entries[1].Length)
	require.Equal(t, int64(2), entries[0].Sequence)
	require.Equal(t, int64(4), entries[1].Sequence)

	entryA1, err := s.Get(entries[0].Reference)
	require.Nil(t, err)
	require.Equal(t, entryA1.Data, []byte("A1_data"))

	entryA2, err := s.Get(entries[1].Reference)
	require.Nil(t, err)
	require.Equal(t, entryA2.Data, []byte("A2_data"))

	// Delete a value
	err = s.Delete(entry.Reference)
	require.Nil(t, err)

	_, err = s.Get(entry.Reference)
	expectedError := &ErrorReferenceNotFound{entry.Reference}
	require.NotNil(t, err)
	require.Equal(t, expectedError.Error(), err.Error())
}

func TestEncodeDecodeSequence(t *testing.T) {
	seq := int64(123456789)

	raw := encodeSequence(seq)
	retrievedSeq := decodeSequence(raw)

	require.Equal(t, seq, retrievedSeq)
}
