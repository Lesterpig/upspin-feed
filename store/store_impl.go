package store

import (
	"strings"
	"time"

	"github.com/dgraph-io/badger"
)

const (
	metaByte     = 0x00
	dataByte     = 0x01
	categoryByte = '/'
)

var sequenceKey = []byte{categoryByte, 's', 'e', 'q', '.', 'i', 'n', 't', 'e', 'r', 'n', 'a', 'l'}

type badgerStore struct {
	db       *badger.DB
	sequence *badger.Sequence
}

// NewStore returns a badger-backed store.
func NewStore(path string) (s Store, err error) {
	opts := badger.DefaultOptions
	opts.Dir = path
	opts.ValueDir = path

	db, err := badger.Open(opts)
	if err != nil {
		return
	}

	seq, err := db.GetSequence(sequenceKey, 100)
	if err != nil {
		return
	}

	// Get a first sequence number for initializatiion
	_, _ = seq.Next()

	s = &badgerStore{db: db, sequence: seq}
	return
}

var listIteratorOptions = badger.IteratorOptions{
	PrefetchValues: false,
	Reverse:        false,
	AllVersions:    false,
}

func (s *badgerStore) Categories() (categories []Category, err error) {
	err = s.db.View(func(txn *badger.Txn) error {
		it := txn.NewIterator(listIteratorOptions)

		// Non-category keys are prefixed by "/".
		// Seeking to '/'+1 = '0' will skip those keys entirely.
		it.Seek([]byte{categoryByte + 1})

		for it.Valid() {
			category := s.getCategory(string(it.Item().Key()))
			raw, err := it.Item().Value()
			if err != nil {
				return err
			}

			sequence, lastUpdate := decodeMeta(raw)
			categories = append(categories, Category{category, lastUpdate, sequence})

			// Seek to the next category
			it.Seek(append([]byte(category), categoryByte+1))
		}

		return nil
	})

	return
}

func (s *badgerStore) List(category string) (entries []Entry, err error) {
	prefix := []byte(s.GetReference(category, ""))

	err = s.db.View(func(txn *badger.Txn) error {
		it := txn.NewIterator(listIteratorOptions)

		for it.Seek(prefix); it.ValidForPrefix(prefix); it.Next() {
			item := it.Item()
			key := item.Key()

			if key[len(key)-1] == metaByte {
				dataKey := append(key[:len(key)-1], dataByte)

				dataItem, err := txn.Get(dataKey)
				if err != nil {
					continue
				}

				rawMeta, err := item.Value()
				if err != nil {
					continue
				}

				sequence, lastUpdate := decodeMeta(rawMeta)

				entries = append(entries, Entry{
					Reference:  string(key[:len(key)-1]),
					LastUpdate: lastUpdate,
					Sequence:   sequence,
					Length:     uint64(dataItem.EstimatedSize() - int64(len(dataItem.Key()))),
				})
			}
		}

		return nil
	})
	return
}

func (s *badgerStore) Get(reference string) (entry Entry, err error) {
	entry.Reference = reference
	metaKey, dataKey := s.getRawKeys(reference)

	var metaItem, dataItem *badger.Item
	var rawMeta, data []byte
	err = s.db.View(func(txn *badger.Txn) error {
		metaItem, err = txn.Get(metaKey)
		if err != nil {
			return err
		}

		rawMeta, err = metaItem.Value()
		if err != nil {
			return err
		}

		dataItem, err = txn.Get(dataKey)
		if err != nil {
			return err
		}

		data, err = dataItem.ValueCopy(nil)
		if err != nil {
			return err
		}

		entry.Sequence, entry.LastUpdate = decodeMeta(rawMeta)
		entry.Length = uint64(len(data))
		entry.Data = data
		return nil
	})

	if err == badger.ErrKeyNotFound {
		err = &ErrorReferenceNotFound{reference}
	}

	return
}

func (s *badgerStore) Put(category, key string, data []byte) (entry Entry, err error) {
	reference := s.GetReference(category, key)
	metaKey, dataKey := s.getRawKeys(reference)
	entry = Entry{
		Reference: reference,
		Category:  category,
		Length:    uint64(len(data)),
		Data:      data,
	}

	err = s.db.Update(func(txn *badger.Txn) error {
		sequence, err := s.sequence.Next()
		if err != nil {
			return err
		}

		entry.Sequence = int64(sequence)
		entry.LastUpdate = time.Now()
		metadata := encodeMeta(entry.Sequence, entry.LastUpdate)

		// Update category directory to latest sequence number
		err = txn.Set(append([]byte(category), categoryByte), metadata)
		if err != nil {
			return err
		}

		// Update key
		err = txn.Set(metaKey, metadata)
		if err != nil {
			return err
		}

		return txn.Set(dataKey, data)
	})

	return
}

func (s *badgerStore) Delete(reference string) error {
	return s.db.Update(func(txn *badger.Txn) error {
		category := s.getCategory(reference)
		sequence, err := s.sequence.Next()
		if err != nil {
			return err
		}

		// Update category directory to latest sequence number
		err = txn.Set(append([]byte(category), categoryByte), encodeSequence(int64(sequence)))
		if err != nil {
			return err
		}

		metaKey, dataKey := s.getRawKeys(reference)
		_ = txn.Delete(metaKey)
		return txn.Delete(dataKey)
	})
}

func (s *badgerStore) Close() error {
	_ = s.sequence.Release()
	return s.db.Close()
}

func (s *badgerStore) GetReference(category, key string) string {
	return category + string(categoryByte) + key
}

func (s *badgerStore) getRawKeys(reference string) (metaKey, dataKey []byte) {
	metaKey = append([]byte(reference), metaByte)
	dataKey = append([]byte(reference), dataByte)
	return
}

func (s *badgerStore) getCategory(key string) string {
	slices := strings.SplitN(key, string(categoryByte), 2)
	if len(slices) == 1 {
		return ""
	}

	return slices[0]
}

func encodeMeta(sequence int64, lastUpdate time.Time) []byte {
	rawTime, _ := lastUpdate.MarshalBinary()
	return append(encodeSequence(sequence), rawTime...)
}

func decodeMeta(raw []byte) (sequence int64, lastUpdate time.Time) {
	sequence = decodeSequence(raw)
	if sequence < 0 {
		return
	}

	_ = lastUpdate.UnmarshalBinary(raw[8:])
	return
}

func encodeSequence(sequence int64) []byte {
	return []byte{
		byte(sequence >> 56),
		byte(sequence >> 48),
		byte(sequence >> 40),
		byte(sequence >> 32),
		byte(sequence >> 24),
		byte(sequence >> 16),
		byte(sequence >> 8),
		byte(sequence),
	}
}

func decodeSequence(raw []byte) int64 {
	if len(raw) < 8 {
		return -1
	}

	return int64(raw[7]) |
		int64(raw[6])<<8 |
		int64(raw[5])<<16 |
		int64(raw[4])<<24 |
		int64(raw[3])<<32 |
		int64(raw[2])<<40 |
		int64(raw[1])<<48 |
		int64(raw[0])<<56
}
