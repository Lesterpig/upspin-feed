// Package store is a low-level utility to store documents
// referenced by a key within a category. It has been
// designed to mainly work with upspin, and provide good
// compatibility with upspin's APIs, especially regarding
// sequence numbers.
package store

import (
	"io"
	"time"
)

// Store shall be used to store generic data into categories,
// list those categories and get original data.
type Store interface {
	io.Closer

	// Read methods
	Categories() ([]Category, error)
	List(category string) ([]Entry, error)
	Get(reference string) (Entry, error)
	GetReference(category, key string) string

	// Write methods
	// Please note that at Put operation shall return a Entry
	// containing a Reference that MUST be used to retrieve the
	// stored data. It is possible to get a reference by using
	// the GetReference function.
	Put(category, key string, data []byte) (Entry, error)
	Delete(reference string) error
}

// Entry contains some information about the data stored in
// a Store.
type Entry struct {
	Reference string
	Category  string

	// LastUpdate, length and sequence may be provided during List and Get calls.
	LastUpdate time.Time
	Length     uint64
	Sequence   int64

	// Data may only provided during Get calls.
	Data []byte
}

// Category contains information about one category.
type Category struct {
	Name       string
	LastUpdate time.Time
	Sequence   int64
}

// ErrorReferenceNotFound shall be returned by Get calls.
type ErrorReferenceNotFound struct {
	reference string
}

func (e *ErrorReferenceNotFound) Error() string {
	return "reference" + e.reference + "not found"
}
