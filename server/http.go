package server

import (
	"html/template"
	"io"
	"net/http"
	"strings"

	"upspin.io/client"
	"upspin.io/path"
	"upspin.io/upspin"
)

type httpServer struct {
	client   upspin.Client
	username string
}

func newHTTPServer(cfg upspin.Config) *httpServer {
	return &httpServer{
		client:   client.New(cfg),
		username: string(cfg.UserName()),
	}
}

var templateFns = template.FuncMap{
	"removeQuestion": func(s string) string {
		return strings.Replace(s, "?", "%3F", -1)
	},
}

var dirHTMLTemplate = template.
	Must(template.New("dir").
		Funcs(templateFns).
		Parse(`<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>
		{{if not .Root}}<a href="../">../</a><br />{{end}}
		{{range .Entries}}<a href="{{. | removeQuestion}}">{{.}}</a><br />
		{{end}}
	</body>
</html>
`))

// Forked from https://github.com/GildasCh/upspin-viewer
func (h *httpServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	filePath, err := path.Parse(upspin.PathName(h.username + "/" + strings.TrimPrefix(r.URL.Path, "/")))
	if err != nil {
		http.Error(w, "Invalid path", http.StatusBadRequest)
		return
	}

	de, err := h.client.Lookup(filePath.Path(), true)
	if err != nil {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}

	if de.IsDir() {
		var entries []*upspin.DirEntry
		entries, err = h.client.Glob(upspin.AllFilesGlob(de.SignedName))
		if err != nil {
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}

		if filePath.NElem() == 0 && len(entries) > 10 {
			entries = entries[len(entries)-10:]
		}

		templateData := struct {
			Entries []string
			Root    bool
		}{
			make([]string, len(entries)),
			filePath.IsRoot(),
		}

		for i, entry := range entries {
			templateData.Entries[i] = strings.TrimPrefix(string(entry.SignedName), h.username+"/")
		}

		_ = dirHTMLTemplate.Execute(w, templateData)
		return
	}

	f, err := h.client.Open(de.SignedName)
	if err != nil {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}

	_, _ = io.Copy(w, f)
}
