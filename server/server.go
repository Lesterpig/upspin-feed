package server

import (
	"net/http"
	"strings"
	"time"

	"lesterpig.com/upspin-feed/feed"
	"lesterpig.com/upspin-feed/store"
	"upspin.io/access"
	"upspin.io/cloud/https"
	"upspin.io/errors"
	"upspin.io/flags"
	"upspin.io/pack"
	"upspin.io/path"
	"upspin.io/rpc/dirserver"
	"upspin.io/rpc/storeserver"
	"upspin.io/serverutil"
	"upspin.io/upspin"
)

var errNotImplemented = errors.E(errors.Invalid, "method not implemented")

const accessString = "read,list: all\n"

// Run starts the store and directory upspin servers
func Run(cfg upspin.Config, s store.Store, f feed.Feed) error {
	srv := &server{
		endpoint: upspin.Endpoint{
			Transport: upspin.Remote,
			NetAddr:   upspin.NetAddr(flags.NetAddr),
		},
		cfg:        cfg,
		store:      s,
		feed:       f,
		accessData: []byte(accessString),
	}

	sources, err := f.GetSources()
	if err != nil {
		return err
	}

	rawSources, err := sources.MarshalBinary()
	if err != nil {
		return err
	}

	srv.setFeedsCache(rawSources)
	srv.accessEntry = srv.getDocumentDirEntry(access.AccessFile, store.Entry{
		Reference:  "access",
		Data:       srv.accessData,
		LastUpdate: time.Now(),
	})

	http.Handle("/api/Dir/", dirserver.New(cfg, &dirServer{srv}, srv.endpoint.NetAddr))
	http.Handle("/api/Store/", storeserver.New(cfg, &storeServer{srv, false}, srv.endpoint.NetAddr))
	http.Handle("/", newHTTPServer(cfg))
	https.ListenAndServeFromFlags(nil)
	return nil
}

type server struct {
	endpoint upspin.Endpoint
	cfg      upspin.Config
	store    store.Store
	feed     feed.Feed

	feedsEntry, accessEntry *upspin.DirEntry
	feedsData, accessData   []byte
}

func (s *server) Endpoint() upspin.Endpoint { return s.endpoint }
func (s *server) Close()                    {}

type storeServer struct {
	*server
	writer bool
}

func (s *storeServer) Dial(cfg upspin.Config, ep upspin.Endpoint) (upspin.Service, error) {
	s2 := *s
	s2.writer = (s.cfg.UserName() == cfg.UserName())
	return &s2, nil
}

func (s *storeServer) Get(ref upspin.Reference) ([]byte, *upspin.Refdata, []upspin.Location, error) {
	if ref == "feeds" {
		return s.feedsData, &upspin.Refdata{
			Reference: ref,
			Volatile:  true,
		}, nil, nil
	}

	if ref == "access" {
		return s.accessData, &upspin.Refdata{
			Reference: ref,
		}, nil, nil
	}

	document, err := s.store.Get(string(ref))
	if err != nil {
		return nil, nil, nil, err
	}

	return document.Data, &upspin.Refdata{
		Reference: ref,
		Volatile:  false,
		Duration:  15 * time.Minute,
	}, nil, nil
}

func (s *storeServer) Put(data []byte) (*upspin.Refdata, error) {
	if !s.writer {
		return nil, errors.E(errors.Private)
	}

	var sources feed.Sources
	err := sources.UnmarshalBinary(data)
	if err != nil {
		return nil, err
	}

	err = s.feed.PutSources(sources)
	if err != nil {
		return nil, err
	}

	s.setFeedsCache(data)
	return &upspin.Refdata{Reference: "feeds", Volatile: true}, nil
}

func (s *storeServer) Delete(ref upspin.Reference) error {
	return errNotImplemented
}

type dirServer struct {
	*server
}

func (s *dirServer) Dial(upspin.Config, upspin.Endpoint) (upspin.Service, error) { return s, nil }

func (s *dirServer) Lookup(name upspin.PathName) (*upspin.DirEntry, error) {
	p, err := path.Parse(name)
	if err != nil {
		return nil, err
	}

	if p.User() != s.cfg.UserName() {
		return nil, errors.E(name, errors.NotExist)
	}

	if p.FilePath() == "feeds.csv" {
		return s.feedsEntry, nil
	}

	if p.FilePath() == access.AccessFile {
		return s.accessEntry, nil
	}

	if p.FilePath() == "today" {
		return s.getDateLink("today"), upspin.ErrFollowLink
	}

	if p.FilePath() == "yesterday" {
		return s.getDateLink("yesterday"), upspin.ErrFollowLink
	}

	switch p.NElem() {
	case 0:
		return &upspin.DirEntry{
			Name:       p.Path(),
			SignedName: p.Path(),
			Attr:       upspin.AttrDirectory,
			Time:       upspin.Now(),
		}, nil
	case 1:
		return &upspin.DirEntry{
			Name:       p.Path(),
			SignedName: p.Path(),
			Attr:       upspin.AttrDirectory,
			Time:       upspin.Now(),
		}, nil
	case 2:
		document, err := s.store.Get(strings.TrimSuffix(p.FilePath(), ".html"))
		if err != nil {
			if _, ok := err.(*store.ErrorReferenceNotFound); ok {
				return nil, errors.E(name, errors.NotExist)
			}
			return nil, err
		}

		return s.getDocumentDirEntry(p.FilePath(), document), nil
	}

	return nil, errors.E(name, errors.NotExist)
}

func (s *dirServer) Put(entry *upspin.DirEntry) (*upspin.DirEntry, error) {
	if entry.Name == upspin.PathName(s.cfg.UserName()+"/feeds.csv") {
		return s.feedsEntry, nil
	}

	return nil, errNotImplemented
}

func (s *dirServer) Glob(pattern string) ([]*upspin.DirEntry, error) {
	return serverutil.Glob(pattern, s.Lookup, s.listDir)
}

func (s *dirServer) listDir(name upspin.PathName) (entries []*upspin.DirEntry, err error) {
	p, err := path.Parse(name)
	if err != nil {
		return
	}

	if p.User() != s.cfg.UserName() {
		err = errors.E(name, errors.NotExist)
		return
	}

	var categories []store.Category
	var documents []store.Entry

	switch p.NElem() {
	case 0: // Root
		entries = []*upspin.DirEntry{
			s.feedsEntry,
			s.getDateLink("today"),
			s.getDateLink("yesterday"),
		}

		categories, err = s.store.Categories()
		if err != nil {
			return
		}

		for _, category := range categories {
			entries = append(entries, &upspin.DirEntry{
				Writer:     s.cfg.UserName(),
				Name:       p.Path() + upspin.PathName(category.Name),
				SignedName: p.Path() + upspin.PathName(category.Name),
				Attr:       upspin.AttrDirectory,
				Time:       upspin.TimeFromGo(category.LastUpdate),
				Sequence:   category.Sequence,
			})
		}
	case 1: // Category
		documents, err = s.store.List(p.Elem(0))
		if err != nil {
			return
		}

		for _, document := range documents {
			entries = append(entries, s.getDocumentDirEntry(document.Reference+".html", document))
		}
	default: // Invalid path
		err = errors.E(name, errors.NotExist)
	}

	return
}

func (s *dirServer) Delete(name upspin.PathName) (*upspin.DirEntry, error) {
	return nil, errNotImplemented
}

func (s *dirServer) WhichAccess(name upspin.PathName) (*upspin.DirEntry, error) {
	return s.accessEntry, nil
}

func (s *dirServer) Watch(name upspin.PathName, sequence int64, done <-chan struct{}) (<-chan upspin.Event, error) {
	return nil, errNotImplemented
}

func (s *server) getDocumentDirEntry(name string, document store.Entry) *upspin.DirEntry {
	username := string(s.cfg.UserName())

	entry := &upspin.DirEntry{
		Writer:     s.cfg.UserName(),
		Name:       upspin.PathName(username + "/" + name),
		SignedName: upspin.PathName(username + "/" + name),
		Packing:    upspin.PlainPack,
		Time:       upspin.TimeFromGo(document.LastUpdate),
		Sequence:   document.Sequence,
	}

	location := upspin.Location{Endpoint: s.endpoint, Reference: upspin.Reference(document.Reference)}

	if document.Data == nil { // No packdata required
		entry.Blocks = []upspin.DirBlock{{
			Location: location,
			Size:     int64(document.Length),
		}}
	} else { // Packdata required
		packer, err := pack.Lookup(upspin.PlainPack).Pack(s.cfg, entry)
		if err != nil {
			panic(err)
		}

		_, _ = packer.Pack(document.Data)
		packer.SetLocation(location)
		err = packer.Close()
		if err != nil {
			panic(err)
		}
	}

	return entry
}

func (s *server) setFeedsCache(raw []byte) {
	s.feedsEntry = s.getDocumentDirEntry("feeds.csv", store.Entry{
		Reference:  "feeds",
		LastUpdate: time.Now(),
		Data:       raw,
	})
	s.feedsData = raw
}

func (s *server) getDateLink(name string) *upspin.DirEntry {
	username := string(s.cfg.UserName())

	return &upspin.DirEntry{
		Writer:     s.cfg.UserName(),
		Name:       upspin.PathName(username + "/" + name),
		SignedName: upspin.PathName(username + "/" + name),
		Time:       upspin.TimeFromGo(time.Now().Truncate(time.Hour)),
		Attr:       upspin.AttrLink,
		Link:       upspin.PathName(username + "/" + s.getDateForString(name)),
	}
}

func (s *server) getDateForString(name string) string {
	if name == "today" {
		return time.Now().Format("2006-01-02")
	}

	if name == "yesterday" {
		return time.Now().AddDate(0, 0, -1).Format("2006-01-02")
	}

	return name
}
